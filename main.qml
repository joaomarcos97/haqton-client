/***************************************************************************
**
**  Copyright (C) 2020 by Qmob Solutions <contato@qmob.solutions>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.15
import "./Pages"

import solutions.qmob.haqton 1.0

import "FontAwesome"

ApplicationWindow {
    width: 380
    height: width * 16/9
    visible: true
    title: qsTr("HaQton")
    id: rootWindow

    property color backGroundColor : "#394454"
    property color mainAppColor: "#1abc9c"
    property color blackColor: "#000000"
    property color borderColor: "#1abc9c"
    property color mainTextCOlor: "#f0f0f0"
    property color popupBackGroundColor: "#b44"
    property color popupTextCOlor: "#ffffff"

    FontLoader {id: fontAwesome; name: "fontawesome"; source: "qrc:/FontAwesome/fontawesome-webfont.ttf" }
    FontLoader { id: gameFont; source: "assets/fonts/PocketMonk-15ze.ttf" }
    Audio { source: "assets/audio/Magic-Clock-Shop_Looping.mp3"; loops: Audio.Infinite; autoPlay: true }

    QtObject {
        id: internal
        property int margins: 10
    }

    StackView {
        id: stackView
        focus: true
        anchors.fill: parent
        background: Rectangle {
            color: backGroundColor
        }
    }

    Component.onCompleted: {
        stackView.push("qrc:/Pages/LogInPage.qml")
    }

    function goToHome() {
        stackView.push("qrc:/Pages/Home.qml")
    }
}
