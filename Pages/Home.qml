import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtMultimedia 5.15

import solutions.qmob.haqton 1.0

import "../Components/"
import "../FontAwesome"

Page {
    id: homePage

    signal registerClicked()

    background: Rectangle {
        color: backGroundColor
    }

    PopupTitle {
        id: popuptitle
    }

    ColumnLayout {
        width: parent.width
        anchors { verticalCenter: parent.verticalCenter; verticalCenterOffset: -haqtonLabel.height }
        Label {
            id: haqtonLabel
            Layout.topMargin: 8*internal.margins
            font { family: gameFont.name; pixelSize: 128 }
            Layout.alignment: Qt.AlignHCenter
            color: mainAppColor; text: "HaQton!"
            SequentialAnimation on scale {
                loops: Animation.Infinite
                PropertyAnimation { to: 0.25; duration: 1500; easing.type: Easing.OutElastic }
                PropertyAnimation { to: 1; duration: 1500; easing.type: Easing.OutElastic }
                PauseAnimation { duration: 10000 }
            }
            SequentialAnimation on rotation {
                loops: Animation.Infinite
                PropertyAnimation { to: -45; duration: 1000; easing.type: Easing.OutElastic }
                PropertyAnimation { to: 45; duration: 1000; easing.type: Easing.OutElastic }
                PropertyAnimation { to: 0; duration: 1000; easing.type: Easing.OutElastic }
                PauseAnimation { duration: 10000 }
            }
        }
        GridLayout {
            columns: 2; columnSpacing: internal.margins/2; rowSpacing: internal.margins/2
            Layout.fillWidth: true; Layout.fillHeight: false
            Repeater {
                model: [
                    {
                        icon: Icons.faPlusCircle,
                        text: "Criar uma partida",
                        id: "newgame"
                    },
                    {
                        icon: Icons.faGamepad,
                        text: "Participar de uma partida",
                        id: "loadgame"
                    },
                    {
                        icon: Icons.faDiceD20,
                        text: "Sobre o HaQton",
                        id: "about"
                    },
                    {
                        icon: Icons.faMedal,
                        text: "Patrocinadores",
                        id: "sponsor"
                    }
                ]
                GameButton {
                    Layout.fillWidth: true; Layout.fillHeight: true
                    iconLabel { text: modelData.icon; color: backGroundColor }
                    text: modelData.text

                    onClicked: {

                        switch (modelData.id) {
                        case "newgame":
                            popuptitle.open()
                            break
                        case "loadgame":
                            break
                        case "about":
                            break
                        case "sponsor":
                            break
                        }
                    }
                }
            }
        }
    }
}
