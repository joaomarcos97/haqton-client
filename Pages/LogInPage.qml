import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
//import Fluid.Controls 1.1

import "../Components"

Page {
    id: loginPage
    property alias textEmail : loginEmail.text
    property alias textNickname: loginNickname.text

    signal registerClicked()

    background: Rectangle {
        color: backGroundColor
    }

    Rectangle {
        id: iconRect
        width: parent.width
        height: parent.height / 3
        color: backGroundColor

        Text {
            id: icontext
            text: qsTr("HaQton")
            anchors.centerIn: parent
            font { family: gameFont.name; pixelSize: 128 }
            color: mainAppColor
        }
    }

    ColumnLayout {
        width: parent.width
        anchors.top: iconRect.bottom
        spacing: 15

        TextField {
            id: loginNickname
            placeholderText: qsTr("Nickname")
            Layout.preferredWidth: parent.width - 20
            Layout.alignment: Qt.AlignHCenter
            color: mainTextCOlor
            font.pointSize: 14
            font.family: "fontawesome"
            leftPadding: 30
            background: Rectangle {
                implicitWidth: 200
                implicitHeight: 50
                radius: implicitHeight / 2
                color: "transparent"

                Text {
                    text: "\uf007"
                    font.pointSize: 14
                    font.family: "fontawesome"
                    color: mainAppColor
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    leftPadding: 10
                }

                Rectangle {
                    width: parent.width - 10
                    height: 1
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    color: mainAppColor
                }
            }
        }

        TextField {
            id: loginEmail
            placeholderText: qsTr("E-mail")
            Layout.preferredWidth: parent.width - 20
            Layout.alignment: Qt.AlignHCenter
            color: mainTextCOlor
            font.pointSize: 14
            font.family: "fontawesome"
            leftPadding: 30
            echoMode: TextField.Normal
            validator: RegExpValidator {
                id: emailValidator
                regExp:/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/
            }

            background: Rectangle {
                implicitWidth: 200
                implicitHeight: 50
                radius: implicitHeight / 2
                color: "transparent"
                Text {
                    text: "\uf0e0"
                    font.pointSize: 14
                    font.family: "fontawesome"
                    color: mainAppColor
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    leftPadding: 10
                }

                Rectangle {
                    width: parent.width - 10
                    height: 1
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    color: mainAppColor
                }
            }
            //            onTextChanged: {
            //                console.log(acceptableInput)
            //            }
        }

        Item {
            height: 20
        }

        Button {
            id: control
            font.pointSize: 16
            baseColor: "transparent"
            borderColor: mainAppColor
            height: 50
            Layout.preferredWidth: loginPage.width - 20
            Layout.alignment: Qt.AlignHCenter

            property alias name: control.text
            property color baseColor
            property color borderColor

            contentItem: Text {
                text: "Próximo"
                font: control.font
                color: backGroundColor
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
            }

            background: Rectangle {
                id: bgrect
                implicitWidth: 100
                implicitHeight: 50
                color: mainAppColor
                radius: height/2
                border.color: borderColor
            }

            onClicked: {
                goToHome(); //Dev Mode
//                if ( ! loginEmail.acceptableInput ) {
//                    popup.popMessage = "Formato de E-mail Inválido !"
//                    popup.open()
//                } else if ( ! ( loginNickname.text.length > 0 ) ) {
//                    popup.popMessage = "O Nickname não pode está vazio"
//                    popup.open()
//                } else {
//                    goToHome();
//                }
            }
        }
    }

    Popup {
        id: popup
        property alias popMessage: message.text

        background: Rectangle {
            id: backgroundPopup
            implicitWidth: loginPage.width
            implicitHeight: 60
            color: popupBackGroundColor
        }
        y: (loginPage.height - backgroundPopup.height)
        modal: true
        focus: true
        closePolicy: Popup.CloseOnPressOutside
        Text {
            id: message
            anchors.centerIn: parent
            font.pointSize: 12
            color: popupTextCOlor
        }
        onOpened: popupClose.start()
    }

    Timer {
        id: popupClose
        interval: 2000
        onTriggered: popup.close()
    }
}
