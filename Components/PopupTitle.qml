import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Popup {
    id: popup
    width: 350
    height: 350
    x: (parent.width - popup.width) / 2
    y: (parent.height - popup.height) / 2
    modal: true
    focus: true

    background: Rectangle {
        id: backgroundPopup
        implicitWidth: parent.width
        implicitHeight: parent.height
        color: backGroundColor
    }

    ColumnLayout {

        id: columnlayout
        anchors.fill: parent

        Text {
            id: title
            text: qsTr("INFORME O TÍTULO DA PARTIDA.")
            font.pointSize: 14
            font.family: "fontawesome"
            color: popupTextCOlor
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }

        TextField {
            id: titlePartida
            placeholderText: qsTr("Nome da Pardida")
            color: mainTextCOlor
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            font.pointSize: 14
            font.family: "fontawesome"
            leftPadding: 30
            background: Rectangle {
                implicitWidth: popup.width - 20
                implicitHeight: 50
                radius: implicitHeight / 2
                color: "transparent"

                Text {
                    text: "\uf0c0"
                    font.pointSize: 14
                    font.family: "fontawesome"
                    color: mainAppColor
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    leftPadding: 10
                }

                Rectangle {
                    width: parent.width - 10
                    height: 1
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    color: mainAppColor
                }
            }
        }

        RowLayout {

            id: rowLayout
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Button {
                id: closePopup

                contentItem: Text {
                    text: "Voltar"
                    font: closePopup.font
                    color: backGroundColor
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }

                background: Rectangle {
                    implicitWidth: 50
                    implicitHeight: 30
                    color: mainAppColor
                    radius: height/2
                    border.color: borderColor
                }

                onClicked: {
                    popup.close()
                    titlePartida.clear()
                }
            }

            Button {
                id: nextControl

                contentItem: Text {
                    text: "Próximo"
                    font: nextControl.font
                    color: backGroundColor
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }

                background: Rectangle {
                    implicitWidth: 50
                    implicitHeight: 30
                    color: mainAppColor
                    radius: height/2
                    border.color: borderColor
                }

                onClicked: {
                    if ( titlePartida.text.length <= 0 ){
                        popupError.popMessage = "O Nome da Partida não é Válido !"
                        popupError.open()
                    }
                }

            }
        }
    }

    Popup {
        id: popupError
        property alias popMessage: message.text

        background: Rectangle {
            id: backgroundPopupError
            implicitWidth: popup.width
            implicitHeight: 60
            color: popupBackGroundColor
        }
        y: (popup.height - backgroundPopupError.height)
        x: 0
        modal: true
        focus: true
        closePolicy: Popup.CloseOnPressOutside
        Text {
            id: message
            anchors.centerIn: parent
            font.pointSize: 12
            color: popupTextCOlor
        }
        onOpened: popupClose.start()
    }

    Timer {
        id: popupClose
        interval: 2000
        onTriggered: popupError.close()
    }

    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
}
